# SimulAD

SimulAD is a disease progression model, allowing to learn dynamical relationships between pathological processes at stake during AD based on the analysis of multi-modal and longitudinal data.

<img src="./fig/model_overview.png">

The method was presented in the following paper in which its mathematical foundation is detailed as well as its application on a research cohort. 

*Simulating the outcome of amyloid treatments in Alzheimer's disease from imaging and clinical data*

by  Clément Abi Nader, Nicholas Ayache, Giovanni B. Frisoni, Philippe Robert, Marco Lorenzi.

[[paper]](https://academic.oup.com/braincomms/article/3/2/fcab091/6257436)

BibTeX citation:
```bibtex
@article{10.1093/braincomms/fcab091,
    author = {Abi Nader, Clément and Ayache, Nicholas and Frisoni, Giovanni B and Robert, Philippe and Lorenzi, Marco and for the Alzheimer’s Disease Neuroimaging Initiative},
    title = "{Simulating the outcome of amyloid treatments in Alzheimer's disease from imaging and clinical data}",
    journal = {Brain Communications},
    volume = {3},
    number = {2},
    year = {2021},
    month = {04},
    issn = {2632-1297},
    doi = {10.1093/braincomms/fcab091},
    note = {fcab091},
}
```

In this repository, we provide a tutorial to illustrate how the model works in practice. 
The objective is to walk through the main results presented in the paper and to provide a baseline to re-use the model. 

# Installation 

## GNU/Linux

Install conda:
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh  
bash Miniconda3-latest-Linux-x86_64.sh
```

Download this github repository and move into in:
```bash
git clone git://gitlab.inria.fr/epione/simulad
cd simulad
```

Install the customized python environment:
```bash
conda env create -f environment.yml
```

Activate the python environment:
```bash
conda activate simulad
```

Install the simulad package:
```bash
python setup.py install
```

In order to use the file tutorial notebook, it is necessary to add the simulad environment in the jupyter kernel.
```bash
python -m ipykernel install --user --name=simulad
```

Then the notebook can be open using jupyter notebook or jupyter lab and the __simulad__ kernel needs to be selected in order to run.

## Windows
Download and install conda from: https://docs.conda.io/en/latest/miniconda.html

Download this github repository from: https://gitlab.inria.fr/epione/simulad

Open the Anaconda prompt and move into the github repository previously downloaded.

Deactivate the base environment:
`conda deactivate`

Install the customized python environment:
`conda env create -f environment.yml`

Activate the python environment:
`conda activate simulad`

Install the simulad package:
`python setup.py install`

In order to use the file tutorial notebook, it is necessary to add the simulad environment in the jupyter kernel.
```bash
python -m ipykernel install --user --name=simulad
```

Then the notebook can be open using jupyter notebook or jupyter lab and the __simulad__ kernel needs to be selected in order to run.