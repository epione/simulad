import torch
import numpy as np
import pandas as pd
import copy
import pickle
from itertools import compress
from .plotting_routines import plot_data

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def remove_keys(dx_dict, keys_to_remove):

    for key in keys_to_remove:
        del dx_dict[key]

    return dx_dict

def rename_dict_keys(dx_dict):

    dx_dict['NLconv MCI wAD'] = dx_dict.pop('NL to MCI')
    dx_dict['NLconv AD dem'] = dx_dict.pop('NL to AD')
    dx_dict['MCI wAD'] = dx_dict.pop('MCI')
    dx_dict['MCIconv wAD'] = dx_dict.pop('MCI to AD')
    dx_dict['AD dem'] = dx_dict.pop('AD')

    return dx_dict

def assign_indices_to_array(array_to_check, reference_array, i):

    index_list = []

    for el in array_to_check:
        if el in reference_array:
            index_list.append(len(reference_array)*i + np.argwhere(reference_array == el)[0][0])

    return index_list

def compute_time_mask(df, time_batch):

    reference_time = time_batch['int_time']
    ids = time_batch['ids']

    time_mask = []

    for i, id in enumerate(ids):

        if isinstance(df.loc[id], pd.DataFrame):
            rid_time = df.loc[id]['VISCODE'].to_numpy()
        else:
            rid_time = np.array([df.loc[id]['VISCODE']])

        time_mask.append(assign_indices_to_array(rid_time, reference_time, i))

    time_mask = np.concatenate(time_mask)

    return torch.from_numpy(time_mask).long().to(device)

def identify_batch_subjects(df, batched_data):

    df_copy = copy.deepcopy(df)
    pos_df = df_copy[df.VISCODE>=0]
    neg_df = df_copy[df.VISCODE<=0]

    #Remove subjects with only a baseline visit from the negative df
    for rid_neg in neg_df.index.unique():
        if isinstance(neg_df.loc[rid_neg], pd.Series):
            neg_df.drop(labels=rid_neg, axis=0, inplace=True)

    #Remove fully negative subjects from the positive df
    for rid_pos in pos_df.index.unique():
        if df_copy.loc[rid_pos].VISCODE.max()==0 and df_copy.loc[rid_pos].VISCODE.min()<0:
            pos_df.drop(labels=rid_pos, axis=0, inplace=True)


    #Remove the baseline point in the negative df for subjects who are both on the positive and negative sides
    cross_ids = [v for v in pos_df.index.unique() if v in neg_df.index.unique()]
    neg_df = neg_df[~((neg_df.index.isin(cross_ids)) & (neg_df.VISCODE==0))]

    df_pos_copy = copy.deepcopy(pos_df)
    df_neg_copy = copy.deepcopy(neg_df)

    for b in batched_data:
        time = batched_data[b]['int_time']
        min = int(np.min(time))
        max = int(np.max(time))

        if min<0:
            temp_df = neg_df
        else:
            temp_df = pos_df

        subset = temp_df[ (temp_df.VISCODE.min(level='RID') >= min) & (temp_df.VISCODE.max(level='RID') <= max) ]
        ids = subset.index.unique().to_list()
        batched_data[b]['ids'] = ids

        temp_df.drop(labels=ids, axis=0, inplace=True)

    to_pop = []

    for b in batched_data:
        if len(batched_data[b]['ids'])==0:
            to_pop.append(b)

    for b in to_pop:
        batched_data.pop(b)

    return batched_data, df_pos_copy, df_neg_copy

def load_data_training(df_path, dx_dict_path, modalities, time_batches, rename=False):

    """The function is specifically defined to load data where viscodes can be negative.
    A simpler version should be written to address the case of only positive dataframes."""

    #Load data, get diagnosis of subjects
    df = pd.read_csv(df_path, index_col='RID')

    df['VISCODE'] = df['VISCODE'] / 12.
    temp_dict = pickle.load(open(dx_dict_path, 'rb'))

    #plot_data(df)

    if rename:
        temp_dict = rename_dict_keys(temp_dict)
        temp_dict['NLconv'] = temp_dict['NLconv MCI wAD'] + temp_dict['NLconv AD dem']
        temp_dict.pop('NLconv MCI wAD')
        temp_dict.pop('NLconv AD dem')

    #Re-order keys of dict
    desired_order = ['NL', 'NL to MCI', 'NLconv MCI wAD', 'NL to AD', 'NLconv AD dem', 'MCI nAD', 'MCI wAD',
                     'MCIconv nAD', 'MCIconv wAD', 'nAD dem', 'AD dem', 'Other', 'NA']

    dx_dict = {}
    for dx in desired_order:
        if dx in list(temp_dict.keys()):
            dx_dict[dx] = temp_dict[dx]

    df_idx = df.index.unique()

    for key in dx_dict:
        mask = list(df_idx.isin(dx_dict[key]))
        filtered_ids = list(compress(df_idx.to_list(), mask))
        dx_dict[key] = filtered_ids

    #Compute mean, std dev, normalize dataframe
    df_mean = df.iloc[:, 1:].mean()
    df_std = df.iloc[:, 1:].std()
    df.iloc[:, 1:] = df.iloc[:, 1:].sub(df_mean).div(df_std)

    # Split data into batches.
    # Batches are built such that patients have integration between [0, t] or [-t, 0],
    # where t varies from the minimum viscode to the maximum one
    time_scale = np.sort(df.VISCODE.unique())
    batched_data = {}

    for interval in time_batches:

        i = interval[0]
        j = interval[1]

        batched_data.update({f'group_{i}_{j}' : {}})
        time = time_scale[(time_scale>=i) & (time_scale<=j)]
        batched_data[f'group_{i}_{j}']['int_time'] = time
        batched_data[f'group_{i}_{j}']['time_mask'] = None
        batched_data[f'group_{i}_{j}'].update({mod : {'follow_up' : None, 'baseline': None,
                                                'mask' : None} for mod in modalities})

    #Assign each subject to its time batch
    #Identify subjects who have both positive and negative viscodes
    batched_data, df_pos, df_neg = identify_batch_subjects(df, batched_data)

    #Computing the time mask for each reference integration array
    for b in batched_data:

        if np.min(batched_data[b]['int_time'])<0:
            df_to_pass = df_neg
        else:
            df_to_pass = df_pos

        time_mask = compute_time_mask(df_to_pass, batched_data[b])
        batched_data[b]['time_mask'] = time_mask

    #Assembling data per batch and modality
    for b in batched_data:

        if np.min(batched_data[b]['int_time'])<0:
            df_to_pass = df_neg
        else:
            df_to_pass = df_pos

        batched_data[b]['int_time'] = torch.from_numpy(batched_data[b]['int_time']).float().to(device)


        for mod in modalities:
            batch_ids = batched_data[b]['ids']
            markers = [c for c in df.columns if mod in c]
            sub_df = df_to_pass[markers].loc[batch_ids]
            sub_df_bl = df[df.VISCODE==0][markers].loc[batch_ids]
            mask = ~sub_df.isnull()

            batched_data[b][mod]['baseline'] = torch.from_numpy(sub_df_bl.to_numpy()).float().to(device)
            batched_data[b][mod]['follow_up'] = torch.from_numpy(sub_df.to_numpy()).float().to(device)
            batched_data[b][mod]['mask'] = torch.from_numpy(mask.to_numpy()).bool().to(device)


    return batched_data, dx_dict

def load_data_checking(df_path, dx_dict_path, modalities, rename=False):

    """The function also uses the df to pre-proces the data but the output is designed to fit what's needed in the
    checking class --> Only baseline points and diagnosis."""

    #Load data, get diagnosis of subjects
    df = pd.read_csv(df_path, index_col='RID')

    df['VISCODE'] = df['VISCODE'] / 12.
    temp_dict = pickle.load(open(dx_dict_path, 'rb'))

    if rename:
        temp_dict = rename_dict_keys(temp_dict)
        temp_dict['NLconv'] = temp_dict['NLconv MCI wAD'] + temp_dict['NLconv AD dem']
        temp_dict.pop('NLconv MCI wAD')
        temp_dict.pop('NLconv AD dem')

    # Re-order keys of dict
    desired_order = ['NL', 'NLconv', 'NL to MCI', 'NLconv MCI wAD', 'NL to AD', 'NLconv AD dem', 'MCI nAD', 'MCI wAD',
                     'MCIconv nAD', 'MCIconv wAD', 'nAD dem', 'AD dem', 'Other', 'NA']

    dx_dict = {}
    for dx in desired_order:
        if dx in list(temp_dict.keys()):
            dx_dict[dx] = temp_dict[dx]

    df_idx = df.index.unique()

    for key in dx_dict:
        mask = list(df_idx.isin(dx_dict[key]))
        filtered_ids = list(compress(df_idx.to_list(), mask))
        dx_dict[key] = filtered_ids

    if 'Unnamed: 0' in df.columns:
        df.drop(columns='Unnamed: 0', inplace=True)

    #Compute mean, std dev, normalize dataframe
    df_mean = df.iloc[:, 1:].mean()
    df_std = df.iloc[:, 1:].std()
    df.iloc[:, 1:] = df.iloc[:, 1:].sub(df_mean).div(df_std)

    #Store mean and std dev in dict
    stats_dict = {mod: {'mu': None, 'std':None} for mod in modalities}

    for mod in modalities:
        markers = [c for c in df_mean.index.to_list() if mod in c]
        mean = torch.from_numpy(df_mean[markers].to_numpy()).float().to(device)
        std = torch.from_numpy(df_std[markers].to_numpy()).float().to(device)
        stats_dict[mod]['mu'] = mean
        stats_dict[mod]['std'] = std

    data_per_dx = {dx : { mod : {'baseline': None} for mod in modalities} for dx in dx_dict}

    for dx in dx_dict:
        ids = dx_dict[dx]
        for mod in modalities:
            markers = [c for c in df.columns if mod in c]
            df_bl = df[df.VISCODE==0][markers].loc[ids]
            data_per_dx[dx][mod]['baseline'] = torch.from_numpy(df_bl.to_numpy()).float().to(device)

    data_per_dx.update({'all_dx' : {mod : {'baseline': None} for mod in modalities }})

    for mod in modalities:
        markers = [c for c in df.columns if mod in c]
        df_bl = df[df.VISCODE == 0][markers]
        data_per_dx['all_dx'][mod]['baseline'] = torch.from_numpy(df_bl.to_numpy()).float().to(device)

    return data_per_dx, stats_dict, dx_dict