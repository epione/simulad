import os
from setuptools import setup, find_packages

src_dir = os.path.join(os.getcwd(), 'src')
packages = {"": "src"}
for package in find_packages("src"):
    packages[package] = "src"

setup(
    packages=packages.keys(),
    package_dir={"": "src"},
    name='simulad',
    version='1.0.0',
    author='Clement Abi Nader',
    author_email='clement.abi-nader@inria.fr',
    description='SimulAD',
    long_description='TODO',
    license='Inria',
    )

