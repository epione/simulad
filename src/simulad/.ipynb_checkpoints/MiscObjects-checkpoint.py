import os
import torch
import matplotlib.pyplot as plt
import numpy as np
import pickle
import shutil
from .plotting_routines import plot_latent_trajectory, boxplot_time_shift, gmm_plot
from torchdiffeq import odeint
from .data_loading import load_data_training, load_data_checking, remove_keys
import pandas as pd
from statsmodels.stats.power import TTestIndPower
from scipy.stats import ttest_ind
from sklearn import mixture
from copy import deepcopy

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class Training:

    def __init__(self, n_epochs, lr, model_name, output_dir):

        self.n_epochs = n_epochs
        self.lr = lr

        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        self.output_dir = output_dir

    def fit_model(self, model, model_name, batched_data):

        optimizer =  torch.optim.Adam(model.parameters(), lr=self.lr)

        for epoch in range(self.n_epochs):

            optimizer.zero_grad()
            loss = model(batched_data)
            loss.backward()
            optimizer.step()

            if epoch % 25 ==0:
                print("Epoch {}, Loss = {}" .format(epoch, loss.detach().item()))

        model.plot_loss(self.output_dir)

        torch.save(optimizer.state_dict(), self.output_dir + '/' + 'optimizer')
        torch.save(model.state_dict(), self.output_dir + '/' + model_name)

        with open(self.output_dir + '/' + 'param.txt', 'w') as f:
            for name, param in model.named_parameters():
                f.write(str(name) + ' : ' + str(param) + '\n')
            f.close()

class Checking:

    """
    This class receives as attributes the trained model, the baseline data per dx,
     the mean and std dev of all features and the main output dir.
    All the plotting routines will be called here in order to check the results.
    """

    def __init__(self, model, data_per_dx, dx_dict, stats_dict, output_dir):

        if not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        self.output_dir = output_dir

        self.model = model
        self.data_per_dx = data_per_dx
        self.dx_dict = dx_dict
        self.stats_dict = stats_dict
        self.conversion_shift = 0.

        self.path_markers = '/user/cabinade/home/Desktop/myPhD/LatentODE/ADNI'

    def latent_trajectory_gmm(self, gmm, n, time_pos, time_neg):

        out_dir = self.output_dir + '/' + 'latent_space'

        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)

        z_stats_temp = torch.zeros(n, len(time_pos) + len(time_neg) -1, self.model.latent_dim)

        for i in range(n):
            z0, _ = gmm.sample(n_samples=100)
            z0 = torch.from_numpy(z0).float()

            if len(time_neg)==1:
                pred_z = odeint(self.model.latent_ode, z0, time_pos, method=self.model.integration_method).permute(1, 0, 2)

            else:
                pred_z_pos = odeint(self.model.latent_ode, z0, time_pos, method=self.model.integration_method).permute(1, 0, 2)
                pred_z_neg = odeint(self.model.latent_ode, z0, time_neg, method=self.model.integration_method).permute(1, 0, 2)
                pred_z = torch.cat((torch.flip(pred_z_neg, dims=[1])[:,:-1,:], pred_z_pos), dim=1)


            if len(time_neg)==1:
                time = time_pos.cpu().detach().numpy()
                time_torch = torch.from_numpy(time).float()

            else:
                time = np.concatenate((time_neg.cpu().detach().numpy()[::-1][:-1], time_pos.cpu().detach().numpy()))
                time_torch = torch.from_numpy(time).float()

            z_stats_temp[i,:,:] = torch.mean(pred_z, dim=0)

        z_stats_temp_np = z_stats_temp.detach().numpy()

        z_stats = np.zeros((3, z_stats_temp_np.shape[1], z_stats_temp_np.shape[2]))
        z_stats[0] = np.mean(z_stats_temp_np, axis=0)
        std = np.std(z_stats_temp_np, axis=0)
        z_stats[1] = z_stats[0] + 1.5*std
        z_stats[2] = z_stats[0] - 1.5*std

        z_stats = torch.from_numpy(z_stats).float()

        return z_stats_temp, z_stats

    def block_amyloid(self, sampled_z_trajectories, time_pos, time_neg, intervention_time):

        time = np.concatenate((time_neg.cpu().detach().numpy()[::-1][:-1], time_pos.cpu().detach().numpy()))
        time = torch.from_numpy(time).float()
        
        pos = torch.argmin(time - intervention_time)

        pred_z_block = odeint(self.model.latent_ode.block_modality, sampled_z_trajectories[:,pos,:], time[pos:], method=self.model.integration_method).permute(1, 0, 2)

        pred_z_block_np = pred_z_block.detach().numpy()

        z_stats = np.zeros((3, pred_z_block_np.shape[1], pred_z_block_np.shape[2]))
        z_stats[0] = np.mean(pred_z_block_np, axis=0)
        std = np.std(pred_z_block_np, axis=0)
        z_stats[1] = z_stats[0] + 1.*std
        z_stats[2] = z_stats[0] - 1.*std

        return z_stats
        
    def drug_intervention(self, gmm, time_pos, time_neg, n, title):

        clinical_scores = ['CDRSB', 'ADAS11', 'MMSE', 'RAVLT immediate', 'RAVLT learning', 'RAVLT forgetting', 'FAQ']

        if len(time_neg) == 1:
            time = time_pos.cpu().detach().numpy()
            time_torch = torch.from_numpy(time).float()

        else:
            time = np.concatenate((time_neg.cpu().detach().numpy()[::-1][:-1], time_pos.cpu().detach().numpy()))
            time_torch = torch.from_numpy(time).float()

        reference, _ = gmm.sample(n_samples=n)
        reference = torch.from_numpy(reference).float()
        ref_pos = np.abs(time - self.conversion_shift).argmin()

        if len(time_neg) == 1:
            pred_reference = odeint(self.model.latent_ode, reference, time_pos, method=self.model.integration_method).permute(1, 0, 2)

        else:
            pred_reference_pos = odeint(self.model.latent_ode, reference, time_pos, method=self.model.integration_method).permute(1, 0, 2)
            pred_reference_neg = odeint(self.model.latent_ode, reference, time_neg, method=self.model.integration_method).permute(1, 0, 2)
            pred_reference = torch.cat((torch.flip(pred_reference_neg, dims=[1])[:, :-1, :], pred_reference_pos), dim=1)

        clinical_reference = self.stats_dict['CLINIC']['mu'] + \
                             self.stats_dict['CLINIC']['std'] * self.model.decoder(pred_reference[:, ref_pos, :])['CLINIC']
        clinical_reference = clinical_reference.cpu().detach().numpy()

        clinical_dict = [{} for _ in range(len(clinical_scores))]

        for i, t in enumerate(time_torch[:ref_pos - 1] - float(self.conversion_shift)):
            simulated_evolution = odeint(self.model.latent_ode.block_modality, pred_reference[:, i, :], time_torch[i:ref_pos + 1], method=self.model.integration_method).permute(1, 0, 2)

            observed_clinical = self.model.decoder(simulated_evolution[:, -1, :])['CLINIC']
            observed_clinical = self.stats_dict['CLINIC']['mu'] + self.stats_dict['CLINIC']['std'] * observed_clinical
            observed_clinical = observed_clinical.cpu().detach().numpy()

            for j, clinic in enumerate(clinical_scores):
                _, p_value = ttest_ind(clinical_reference[:, j], observed_clinical[:, j])
                power_analysis = TTestIndPower()
                effect_size = np.abs(np.mean(clinical_reference[:, j]) - np.mean(observed_clinical[:, j])) / np.std(
                    np.concatenate((clinical_reference[:, j], observed_clinical[:, j])))
                power = power_analysis.power(effect_size, len(clinical_reference[:, j]), 0.01, 1.)
                sample_size = power_analysis.solve_power(effect_size, None, 0.01, 0.8, 1.)
                final_value = (np.mean(observed_clinical[:, j], axis=0), np.std(observed_clinical[:, j], axis=0))
                delta = ( np.mean( observed_clinical[:, j] - clinical_reference[:,j], axis=0 ), \
                         np.std( observed_clinical[:, j] - clinical_reference[:,j], axis=0) )

                clinical_dict[j].update({float(t.numpy()): {'p-value_' + clinic: p_value,
                                             'effect_size_' + clinic: effect_size,
                                             'power_' + clinic: power,
                                             'sample_size_' + clinic: sample_size,
                                             'mean_delta_' + clinic: delta[0],
                                             'mean_final_' + clinic: final_value[0],
                                             'std_delta_' + clinic: delta[1],
                                             'std_final_' + clinic: final_value[1]}})


        df_list = []
        for i, mod in enumerate(clinical_scores):
            df_list.append(pd.DataFrame(clinical_dict[i],
                                        index=['p-value_' + mod, 'effect_size_' + mod, 'power_' + mod, 'sample_size_' + mod,
                                               'mean_delta_' + mod, 'mean_final_' + mod, 'std_delta_' + mod, 'std_final_' + mod]).T)

        result = pd.concat(df_list, axis=1)
        result.reset_index(inplace=True)
        result.rename(columns={'index' : 'Time'}, inplace=True)

        ratio = self.model.latent_ode.ratio
        result.to_csv(os.path.join('./results', f'{title}.csv'))

        return result

    def pseudo_time_shift(self, z_stats, time_pos, time_neg, title):

        time_shift = {dx: [] for dx in self.dx_dict}
        time_shift_ids = {dx: [] for dx in self.dx_dict}

        time = np.concatenate((time_neg.cpu().detach().numpy()[::-1][:-1], time_pos.cpu().detach().numpy()))
        
        inv_map = {}
        for key in self.dx_dict:
            for v in self.dx_dict[key]:
                inv_map[v] = key

        _, mu_z0, _ = self.model.sampling(self.data_per_dx['all_dx'], False)

        for i, rid in enumerate(sorted(inv_map)):
            diff = torch.sum(torch.abs(z_stats[0] - mu_z0[i, :]), dim=1)
            idx = torch.argmin(diff)
            diag = inv_map[rid]
            time_shift[diag].append(time[idx])
            time_shift_ids[diag].append(rid)

        #MCI subjects converts to Dementia after 2.4 years in average in the ADNI merge.
        #We look at the average time0shift of the MCI converters and add 2.4 years to approximate a reference time of conversion to dementia that we will use as a zero.
        self.conversion_shift = np.mean(time_shift['MCIconv wAD']) + 2.4

        for key in time_shift:
            for i in range(len(time_shift[key])):
                time_shift[key][i] = time_shift[key][i] - self.conversion_shift
            
        return time_shift, time - self.conversion_shift

    def fit_and_select_gmm(self, data_per_dx):

        out_dir = self.output_dir

        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)

        _, mu_z0, _ = self.model.sampling(data_per_dx, False)
        mu_z0_np = mu_z0.detach().numpy()

        lowest_aic = np.infty
        aic = []
        n = [1,2]
        cv_types = ['spherical', 'tied', 'diag', 'full']

        for cv_type in cv_types:
            for n_components in n:
                # Fit a Gaussian mixture with EM
                gmm = mixture.GaussianMixture(
                                              n_components=n_components,
                                              covariance_type=cv_type,
                                              )
                gmm.fit(mu_z0_np)
                aic.append(gmm.aic(mu_z0_np))
                if aic[-1] < lowest_aic:
                    lowest_aic = aic[-1]
                    best_gmm = gmm

        best_gmm.fit(mu_z0_np)
        
        if mu_z0_np.shape[1] == 4:
            gmm_plot(mu_z0_np, best_gmm, out_dir)

        return best_gmm

    def data_space_trajectory(self, pred_z, time):

        if not isinstance(time, np.ndarray):
            time = time.detach().cpu().numpy()
        
        pred_x = self.model.decoder(pred_z)

        #We show here how to compute the values of the clinincal scores but don't provide a plotting function
        #The clinical scores evolution is shown along with the imaging evolution 
        x_clinic = pred_x['CLINIC']
        x_clinic = self.stats_dict['CLINIC']['mu'] + x_clinic * self.stats_dict['CLINIC']['std']

        modalities = ['MRI', 'FDG', 'AV45']

        for mod in modalities:
            pred_x[mod] = pred_x[mod][0, :, :]
            pred_x[mod] = torch.abs(pred_x[mod] - pred_x[mod][0, :])

            ##Normalization is performed with respect to cortical regions only
            max_mod = torch.max(pred_x[mod][:,:-7])
            min_mod = torch.min(pred_x[mod][:,:-7])

            ##Values are re-scaled in the range [0, 3] to match the scale used in the brain painter sfotware and create the brain images
            pred_x[mod] = 3 * (pred_x[mod] - min_mod) / (max_mod - min_mod)

            pred_x[mod][:, -7:] = 0.
            pred_x[mod] = pred_x[mod].cpu().detach().numpy()

        roi = []
        with open('../src/DK_roi.txt') as f:
            Lines = f.readlines()
            for l in Lines:
                roi.append(l.rstrip('\n'))

        trajectory = {}
        trajectory.update({'Image-name-unique': []})
        trajectory.update({r: [] for r in roi})

        for mod in modalities:
            data = pred_x[mod]
            for i, t in enumerate(time - self.conversion_shift):
                trajectory['Image-name-unique'].append(mod + '_' + str(t))
                for j, r in enumerate(roi):
                    trajectory[r].append(data[i, j])

        trajectory['unknown'] = [0. for i in range(len(trajectory['bankssts']))]
        trajectory['Inf-Lat-Vent'] = [0. for i in range(len(trajectory['bankssts']))]
        trajectory['Cerebellum-Cortex'] = [0. for i in range(len(trajectory['bankssts']))]
        trajectory['Lateral-Ventricle'] = [0. for i in range(len(trajectory['bankssts']))]
        trajectory['VentralDC'] = [0. for i in range(len(trajectory['bankssts']))]

        trajectory_df = pd.DataFrame(trajectory)
        trajectory_df.to_csv(os.path.join('./results', 'estimated_roi.csv'), index=False)
