import torch
import torch.nn as nn

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class LatentODEfunc(nn.Module):

    def __init__(self, latent_dim, modalities):
        super(LatentODEfunc, self).__init__()

        self.latent_dim = latent_dim
        self.W = nn.Parameter(0.0*torch.randn(self.latent_dim, self.latent_dim) - 0.0, requires_grad=True)
        self.ratio = None
        self.index = None
        self.modalities = modalities

    def forward(self, t, x):
    #The latent dynamical system that we specify for our biomarkers

        V = torch.diag(self.W.diag())
        out = torch.mm(x, self.W.t()) - torch.mm(x**2, V)

        return out

    def block_modality(self, t, x):
    #Integrate the latent dynamical system and blocks the evolution of the modality indexed by self.index with a ratio self.ratio

        V = torch.diag(self.W.diag())
        out = torch.mm(x, self.W.t()) - torch.mm(x**2, V)
        out[:,self.index] = self.ratio * out[:,self.index]

        return out

    def set_ratio(self, ratio):
    #Set the blocking ratio of a modality

        self.ratio = ratio

    def set_blocked_modality(self, modality):
    #Choose the modality to block

        self.index = self.modalities.index(modality)

class Encoder(nn.Module):
    """Encoder module to project each data modality in the latent space via its individual self.net layer"""

    def __init__(self, modalities_dict):
        super(Encoder, self).__init__()

        self.mod_dict = modalities_dict
        self.latent_dim = 1

        self.net = nn.ModuleDict({key : nn.Linear(self.mod_dict[key], 2*self.latent_dim) for key in self.mod_dict})
        self.custom_init_()

    def custom_init_(self):

        for mod in self.mod_dict:
            if isinstance(self.net[mod], nn.Linear):
                nn.init.normal_(self.net[mod].weight, mean=0., std=0.001)
                nn.init.zeros_(self.net[mod].bias)

                nn.init.zeros_(self.net[mod].weight[1,:])
                nn.init.constant_(self.net[mod].bias[1], val=-20.)


    def forward(self, x):

        out = {key : {'mu' : None, 'logvar' : None} for key in self.mod_dict}

        for key in self.mod_dict:
            res = self.net[key](x[key]['baseline'])
            out[key]['mu'], out[key]['logvar'] = res[:, :self.latent_dim], res[:, self.latent_dim:]

        return out

class Decoder(nn.Module):
    """Decoder module to map each the latent varibale to the different measurements for each data modality via individual self.net layers"""

    def __init__(self, modalities_dict):
        super(Decoder, self).__init__()

        self.mod_dict = modalities_dict
        self.latent_dim = len(self.mod_dict)

        self.net = nn.ModuleDict({key : nn.Linear(self.latent_dim, self.mod_dict[key]) for key in self.mod_dict})
        self.custom_init_()

    def custom_init_(self):

        for mod in self.mod_dict:
            if isinstance(self.net[mod], nn.Linear):
                nn.init.sparse_(self.net[mod].weight, sparsity=0.9)
                nn.init.zeros_(self.net[mod].bias)

    def forward(self, z):

        out = {}

        for i, key in enumerate(self.mod_dict):
            out[key] = self.net[key](z)

        return out

