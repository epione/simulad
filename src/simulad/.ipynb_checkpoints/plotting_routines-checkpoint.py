import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
import math
import torch
from sklearn.metrics import auc, roc_curve
from scipy.stats import spearmanr
from copy import deepcopy

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def choose_subplot_dimensions(k):

    if k <= 3:
        return 1, k

    else:

        nrows = math.floor(math.sqrt(k))
        ncols = math.ceil(k / nrows)

    return nrows, ncols

def generate_subplots(k, figsize=(8,6)):

    nrow, ncol = choose_subplot_dimensions(k)

    figure, axes = plt.subplots(nrow, ncol, figsize=figsize)

    if not isinstance(axes, np.ndarray):
        return figure, [axes]
    else:
        axes = axes.flatten(order='C')

        # Delete any unused axes from the figure, so that they don't show
        # blank x- and y-axis lines
        for idx, ax in enumerate(axes[k:]):
            figure.delaxes(ax)

            # Turn ticks on for the last ax in each column, wherever it lands
            idx_to_turn_on_ticks = idx + k - ncol
            for tk in axes[idx_to_turn_on_ticks].get_xticklabels():
                tk.set_visible(True)

        axes = axes[:k]
        return figure, axes

def boxplot_time_shift(time_shift, title):
    
    plt.style.use('ggplot')
    
    box_names = [key for key in time_shift][::-1]
    data = [time_shift[key] for key in time_shift][::-1]

    plt.figure(figsize=(16, 9))

    box = plt.boxplot(data, labels=box_names, whis=[5,95], whiskerprops = dict(linestyle='--'), vert=False)
    plt.title('Estimated disease severity across clinical stages')
    plt.xlabel('Time to AD dementia (years)')
    plt.ylabel('Clinical stages')
    plt.tight_layout()
    plt.savefig(os.path.join('./results',f'{title}.png'))


def plot_latent_trajectory(pred_z, timeline, processes, title, normal_max_min=None):

    plt.style.use('ggplot')
    
    CB_color_cycle = ['#800080', '#0000FF', '#00FF00', '#FF0000', 'm']
    styles = ['solid', 'dotted', 'dashed', 'dashdot', 'loosely dotted']

    if not isinstance(pred_z, np.ndarray):
        pred_z = pred_z.cpu().detach().numpy()

    #Making curves monotinically increasing
    pred_z = pred_z * np.sign(pred_z[0,-1,:] - pred_z[0,0,:])

    plt.figure(figsize=(16,9))

    #Normalizing with repsect to the extreme values of the trajectories obtained when no intervention is performed
    if normal_max_min is None:
        pred_z = (pred_z - np.min(pred_z[0], axis=0)) / (np.max(pred_z[0], axis=0) - np.min(pred_z[0], axis=0))
    else:
        pred_z = (pred_z - np.min(pred_z[0], axis=0)) / (normal_max_min[1] - normal_max_min[0])

    for i, mod in enumerate(processes):
        plt.plot(timeline, pred_z[0,:,i], label=mod, color=CB_color_cycle[i], linewidth=4., linestyle=styles[i])
        plt.fill_between(timeline, pred_z[2,:,i], pred_z[1,:,i] , alpha=0.2, color=CB_color_cycle[i])

    plt.title('z-scores evolution')
    plt.xlabel('Time to AD dementia (years)')
    plt.ylabel('z-value (a.u)')
    plt.legend(fontsize='x-large', handlelength=3)
    plt.tight_layout()
    plt.savefig(os.path.join('./results', f'{title}.png'))


def gmm_plot(mu_z0, gmm, out_dir):

    plt.style.use('ggplot')
    
    modalities = ['$z^{cli}$', '$z^{atr}$', '$z^{met}$', '$z^{amy}$']
    CB_color_cycle = ['#999999', '#dede00', '#4daf4a','#377eb8', '#a65628']
    ax_indices = [(0,0), (0,1), (0,2), (1,0), (1,1), (1,2)]
    mod_indices = [(2,3), (2,1), (2,0), (3,1), (3,0), (1,0)]

    mean = gmm.means_
    covariance = gmm.covariances_

    plt.figure(figsize=(16,10))

    for i, (ax, mod) in enumerate(zip(ax_indices, mod_indices)):

        h = plt.subplot(2,3,i+1)

        plt.scatter(mu_z0[:,mod[0]], mu_z0[:,mod[1]], color='red', label='AD')


        if gmm.covariance_type=='tied':
            cov = covariance[[mod[0], mod[0], mod[1], mod[1]], [mod[0], mod[1], mod[0], mod[1]]].reshape(2,2)
            v, w = np.linalg.eigh(cov)
            # Plot an ellipse to show the Gaussian component

            for n in range(gmm.n_components):

                angle = np.arctan2(w[0][1], w[0][0])
                angle = 180. * angle / np.pi  # convert to degrees
                v = 2. * np.sqrt(2.) * np.sqrt(v)
                ell1 = mpl.patches.Ellipse(mean[n,mod], v[0], v[1], 180. + angle, color='navy')
                ell1.set_clip_box(h.bbox)
                ell1.set_alpha(.5)
                h.add_artist(ell1)


            plt.xlabel(modalities[mod[0]])
            plt.ylabel(modalities[mod[1]])

        elif gmm.covariance_type=='full':

            for n in range(gmm.n_components):

                cov = covariance[n][[mod[0], mod[0], mod[1], mod[1]], [mod[0], mod[1], mod[0], mod[1]]].reshape(2, 2)
                v, w = np.linalg.eigh(cov)

                # Plot an ellipse to show the Gaussian component
                angle = np.arctan2(w[0][1], w[0][0])
                angle = 180. * angle / np.pi  # convert to degrees
                v = 2. * np.sqrt(2.) * np.sqrt(v)
                ell = mpl.patches.Ellipse(mean[n,mod], v[0], v[1], 180. + angle, color='navy')
                ell.set_clip_box(h.bbox)
                ell.set_alpha(.5)
                h.add_artist(ell)


            plt.xlabel(modalities[mod[0]])
            plt.ylabel(modalities[mod[1]])

        elif gmm.covariance_type=='diag':

            for n in range(gmm.n_components):

                cov = np.diag(covariance[n][[mod[0], mod[1]]])
                v, w = np.linalg.eigh(cov)

                # Plot an ellipse to show the Gaussian component
                angle = np.arctan2(w[0][1], w[0][0])
                angle = 180. * angle / np.pi  # convert to degrees
                v = 2. * np.sqrt(2.) * np.sqrt(v)
                ell = mpl.patches.Ellipse(mean[n,mod], v[0], v[1], 180. + angle, color='navy')
                ell.set_clip_box(h.bbox)
                ell.set_alpha(.5)
                h.add_artist(ell)


            plt.xlabel(modalities[mod[0]])
            plt.ylabel(modalities[mod[1]])

    plt.suptitle('Latent projection of AD dementia subjects')
    #plt.tight_layout()
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.savefig(os.path.join(out_dir, 'latent_space_gmm.png'))

def plot_data(df, n=100):

    df_cp = deepcopy(df)

    df_cp['VISCODE'] = df_cp['VISCODE'] / 12.

    plt.style.use('ggplot')
    figure, axes = plt.subplots(4, 4, figsize=(16,16))

    idx = df.index.unique()[:n]
    biomarkers =  ['CDRSB_CLINIC', 'ADAS11_CLINIC', 'MMSE_CLINIC', 'FAQ_CLINIC',
                   'entorhinal_MRI', 'Hippocampus_MRI', 'middletemporal_MRI', 'posteriorcingulate_MRI',
                   'superiorfrontal_AV45', 'posteriorcingulate_AV45', 'inferiorparietal_AV45', 'middletemporal_AV45',
                   'inferiorparietal_FDG', 'posteriorcingulate_FDG', 'inferiortemporal_FDG', 'precuneus_FDG']

    if not isinstance(axes, np.ndarray):
        return figure, [axes]
    else:
        axes = axes.flatten(order='C')


    for i in range(len(axes)):
        for j in range(len(idx)):
            series = df_cp.loc[idx[j]][['VISCODE', biomarkers[i]]].dropna()
            axes[i].plot(series['VISCODE'] - series['VISCODE'].min(), series[biomarkers[i]])

        axes[i].set_xlabel('Time (years)')
        axes[i].set_ylabel('Biomarker value')
        axes[i].set_title(biomarkers[i])

    plt.tight_layout()
    plt.savefig('./data/biomarkers_data.png')

def plot_intervention_effect(df, score, intervention_name, ratio, title):

    plt.style.use('ggplot')

    plt.figure(figsize=(12,8))

    plt.plot(df['Time'], df[f'mean_final_{score}'], label=f'{intervention_name} intervention, gamma = {ratio} (mean)', linewidth=3)
    plt.fill_between(df['Time'], df[f'mean_final_{score}'] + 0.4*df[f'std_final_{score}'],
                             df[f'mean_final_{score}'] - 0.4*df[f'std_final_{score}'], alpha=0.25)

    final_value = df.iloc[-1][f'mean_final_{score}']
    final_std = 0.4*df.iloc[-1][f'std_final_{score}']

    plt.plot(df['Time'], final_value*np.ones(len(df)), color='black', linestyle='-', label='No intervention (mean)', linewidth=3)
    plt.fill_between(df['Time'], (final_value+final_std)*np.ones(len(df)), (final_value-final_std)*np.ones(len(df)), color='black', alpha=0.25)

    plt.xlabel('Intervention time (years before AD dementia onset)')
    plt.ylabel('Score at AD dementia onset')
    plt.legend()
    plt.title(f'Simulated {score} depending on year of {intervention_name} intervention')
    plt.savefig(os.path.join('./results', f'{title}.png'))