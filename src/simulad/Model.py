import torch
import torch.nn as nn
from torchdiffeq import odeint
import numpy as np
import matplotlib.pyplot as plt

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class LatentDynamics(nn.Module):

    def __init__(self, encoder, decoder, latent_ode, sigma0, latent_dim, modalities):

        super(LatentDynamics, self).__init__()

        self.encoder = encoder
        self.decoder = decoder
        self.latent_ode = latent_ode

        self.logvar_noise = nn.Parameter(torch.log(torch.tensor(sigma0)), requires_grad=True)

        self.latent_dim = latent_dim
        self.modalities = modalities
        self.loss_dict = {'Loss' : [], 'p(x|z)' : [], 'KL' : []}

        self.integration_method = 'midpoint'


    def plot_loss(self, output_dir):

        fig, axes = plt.subplots(1,3, figsize=(12,8))

        for i, key in enumerate(self.loss_dict):
            axes[i].plot(np.arange(len(self.loss_dict[key])), self.loss_dict[key])
            axes[i].set_title(key)
            axes[i].set_xlabel('Iterations')

        fig.suptitle('Losses')
        plt.savefig(output_dir + '/' + 'loss.png')
        plt.close()

    def kl(self, mu1, mu2, logvar1, logvar2):

        kl = torch.sum(0.5*(torch.exp(logvar1 - logvar2) \
                  + (mu1 - mu2)**2 / (torch.exp(logvar2)) \
                  - 1. \
                  + logvar2 \
                  - logvar1))

        return kl

    def sampling(self, X, pertubation=True):

        N =  X[self.modalities[0]]['baseline'].size(0) #X[keys[0]]['baseline'].size(0)
        n_modalities = len(self.modalities)#len(keys)

        mu_z0 = torch.Tensor(N, n_modalities).to(device)
        logvar_z0 = torch.Tensor(N, n_modalities).to(device)
        z0 = torch.Tensor(N, n_modalities).to(device)

        mu_logvar_dict = self.encoder(X)

        #eps = torch.randn(N)
        eps = torch.randn(N, self.latent_dim)

        for i, mod in enumerate(self.modalities):

            mu_z0[:, i] = mu_logvar_dict[mod]['mu'][:,0]
            logvar_z0[:, i] = mu_logvar_dict[mod]['logvar'][:,0]

            if pertubation:
                z0[:,i] =  mu_z0[:, i] + torch.exp(0.5*logvar_z0[:, i])*eps[:, i]
            else:
                z0[:,i] = mu_logvar_dict[mod]['mu'][:,0]

        return z0, mu_z0, logvar_z0

    def integrate_trajectory(self, z0, integration_time):

        if torch.min(integration_time)<0:
            integration_time = torch.flipud(integration_time)

        pred_z = odeint(self.latent_ode, z0, integration_time, method=self.integration_method).permute(1, 0, 2)

        if torch.min(integration_time) < 0:
            pred_z = torch.flip(pred_z, dims=[1])

        return pred_z


    def forward(self, batched_data):

        total_log_pxz = torch.tensor(0.).to(device)

        mu_z0_list = []
        logvar_z0_list = []

        #Computing the expectation term
        for batch in batched_data:

            X = batched_data[batch]
            integration_time = X['int_time']
            time_mask = X['time_mask']

            z0, mu_z0, logvar_z0 = self.sampling(X, True)

            mu_z0_list.append(mu_z0)
            logvar_z0_list.append(logvar_z0)

            pred_z = self.integrate_trajectory(z0, integration_time)
            pred_z = pred_z.reshape(pred_z.size(0) * pred_z.size(1), pred_z.size(2))
            pred_z = pred_z[time_mask]

            pred_x = self.decoder(pred_z)
            log_pxz_mod = torch.Tensor(len(self.modalities))
            const = torch.from_numpy(np.array([2. * np.pi])).float().to(device)

            for i, mod in enumerate(self.modalities):
                mask = X[mod]['mask']
                masked_pred_x = pred_x[mod][mask]
                masked_X = X[mod]['follow_up'][mask]
                l = len(masked_pred_x)
                log_pxz_mod[i] = (- 0.5*l*torch.log(const) - 0.5*l*self.logvar_noise[i] \
                                  - 0.5 * torch.exp(-self.logvar_noise[i]) * torch.sum((masked_pred_x - masked_X)**2))

            log_pxz = torch.sum(log_pxz_mod)
            total_log_pxz = total_log_pxz + log_pxz


        #Computing KL
        mu_z0 = torch.cat(mu_z0_list, dim=0)
        logvar_z0 = torch.cat(logvar_z0_list, dim=0)
        kl = self.kl(mu_z0, torch.zeros(mu_z0.size()).to(device), logvar_z0, torch.zeros(logvar_z0.size()).to(device))


        #Total loss
        loss = -(total_log_pxz - kl)
        cur_loss = [loss.item(), total_log_pxz.item(), kl.item()]

        for i, key in enumerate(self.loss_dict):
            self.loss_dict[key].append(cur_loss[i])

        return loss
